import { Text, Timeline, Title } from "@mantine/core"
import { Check } from "tabler-icons-react"

function PersonalProgress() {
    return (
        <>
            <Title order={2} pb='md'>Timeline</Title>
            <Timeline active={0} lineWidth={3}>
                <Timeline.Item title='Personal website' lineVariant='dotted' bullet={<Check />}>
                    <Text color='dimmed' size='sm'>Still work in progress</Text>
                </Timeline.Item>
                <Timeline.Item title='Switch to Astro'>
                    <Text color='dimmed' size='sm'>Port this site to Astro</Text>
                </Timeline.Item>
                <Timeline.Item title='Full auth project'>
                    <Text color='dimmed' size='sm'>A full stack .NET web application</Text>
                </Timeline.Item>
                <Timeline.Item title='Godot 4 Demo'>
                    <Text color='dimmed' size='sm'>Godot 4 game including custom assets and textures</Text>
                </Timeline.Item>
            </Timeline>
        </>
    )
}

export default PersonalProgress