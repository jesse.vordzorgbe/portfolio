import { Badge, Grid, Group, Image, Paper, Stack, Text, Title } from "@mantine/core"

import ProjectData from "../types/ProjectData"

type props = {
    project: ProjectData
}

function ProjectCard({ project }: props) {
    return (
        <Paper component='a' href={project.url} target='_blank' shadow='sm' withBorder>
            <Grid gutter={0}>
                <Grid.Col xl={4} sx={{ marginBottom: 0 }}>
                    <Image src={project.image} radius='sm' height={200} />
                </Grid.Col>
                <Grid.Col xl={8}>
                    <Stack p='md' spacing='xs' style={{ flexGrow: 1 }}>
                        <Group position='apart' pb='md'>
                            <Title order={1} size='h4'>{project.title}</Title>
                            <Group>
                                {project.technologies.map((item, index) => (
                                    <Badge key={index}>{item}</Badge>
                                ))}
                            </Group>
                        </Group>
                        <Text>{project.description}</Text>
                    </Stack>
                </Grid.Col>
            </Grid>
        </Paper>
    )
}

export default ProjectCard