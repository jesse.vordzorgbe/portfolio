import { Grid, Group, Image, Paper, Skeleton, Stack } from "@mantine/core"

function ProjectCardPlaceholder() {
    return (
        <Paper component='a' target='_blank' shadow='sm' withBorder>
            <Grid gutter={0}>
                <Grid.Col xl={4} sx={{ marginBottom: 0 }}>
                    <Image withPlaceholder radius='sm' height={200} />
                </Grid.Col>
                <Grid.Col xl={8}>
                    <Stack p='md' spacing='xs' style={{ flexGrow: 1 }}>
                        <Group position='apart' pb='md'>
                            <Skeleton height={12} width={150} my='xs' />
                            <Group>
                                <Skeleton height={12} width={75} my='xs' />
                                <Skeleton height={12} width={50} my='xs' />
                                <Skeleton height={12} width={100} my='xs' />
                            </Group>
                        </Group>
                        <Skeleton height={8} />
                        <Skeleton height={8} />
                        <Skeleton height={8} />
                        <Skeleton height={8} width={250} />
                    </Stack>
                </Grid.Col>
            </Grid>
        </Paper>
    )
}

export default ProjectCardPlaceholder