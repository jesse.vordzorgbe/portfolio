import { Container, Text, Title } from "@mantine/core"

import Layout from "../layouts/Layout"

function Interests() {

    return (
        <Layout>
            <Container size='xl'>
                <Title order={1} py='md'>Interests</Title>
                <Text>
                    This was supposed to be a section where I displayed my current interests but I think it would be more interesting
                    to keep a blog where I post updates of what I'm actively working on.
                    It seems like a nice way to track my progress / learning journey and document my growth as I work on various things throughout the years.
                </Text>
            </Container>
        </Layout>
    )
}

export default Interests