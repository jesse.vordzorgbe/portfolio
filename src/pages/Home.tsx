import { Link } from 'react-router-dom';
import { createStyles, Text, Button, Container, Group } from '@mantine/core';
import { FileDownload } from 'tabler-icons-react';

import Layout from "../layouts/Layout"

const BREAKPOINT_MD = '@media (max-width: 992px)';
const BREAKPOINT_SM = '@media (max-width: 768px)';

const useStyles = createStyles((theme) => ({
    wrapper: {
        position: 'relative',
        boxSizing: 'border-box',
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.white,
    },

    inner: {
        position: 'relative',
        paddingTop: 100,
        paddingBottom: 120,

        [BREAKPOINT_MD]: {
            paddingBottom: 50,
            paddingTop: 80,
        },
        [BREAKPOINT_SM]: {
            paddingTop: 20,
        }
    },

    title: {
        fontFamily: `Greycliff CF, ${theme.fontFamily}`,
        fontSize: 52,
        fontWeight: 900,
        lineHeight: 1.1,
        margin: 0,
        padding: 0,

        [BREAKPOINT_MD]: {
            fontSize: 34,
            lineHeight: 1.2,
        },
        [BREAKPOINT_SM]: {
            fontSize: 24
        }
    },

    description: {
        marginTop: theme.spacing.xl,
        fontSize: 20,

        [BREAKPOINT_MD]: {
            marginTop: theme.spacing.xs,
            fontSize: 18,
        },
    },

    controls: {
        marginTop: theme.spacing.xl * 2,

        [BREAKPOINT_MD]: {
            marginTop: theme.spacing.xl,
        },
    },

    control: {
        height: 48,
        paddingLeft: 38,
        paddingRight: 38,

        [BREAKPOINT_MD]: {
            height: 48,
            paddingLeft: 18,
            paddingRight: 18,
            flex: 1,
        },
    },
}))

function Home() {
    const { classes } = useStyles();
    return (
        <Layout>
            <Container size='xl' className={classes.inner}>
                <h1 className={classes.title}>
                    Hi! I'm{' '}
                    <Text component="span" variant="gradient" gradient={{ from: 'blue', to: 'cyan' }} inherit>
                        Jesse Vordzorgbe
                    </Text>{' '}
                </h1>

                <Text className={classes.description} color="dimmed">
                    Welcome to my personal website! I'm Jesse Vordzorgbe and I'm currently a student at Karel de Grote University College.
                    On this site, you'll find my projects, interests, and resume.
                    I'm always looking for new opportunities and experiences,
                    so feel free to get in touch with me if you think I might be a good fit for your team.
                </Text>

                <Group className={classes.controls}>
                    <Button
                        component={Link}
                        to='/contact'
                        size="xl"
                        className={classes.control}
                        variant="gradient"
                        gradient={{ from: 'blue', to: 'cyan' }}
                    >
                        Get in touch
                    </Button>

                    <Button
                        component={Link}
                        to="/resume"
                        size="xl"
                        variant="default"
                        className={classes.control}
                        leftIcon={<FileDownload size={20} />}
                    >
                        Resume
                    </Button>
                </Group>
            </Container>
        </Layout>

    )
}

export default Home