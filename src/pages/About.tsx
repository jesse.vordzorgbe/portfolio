import { Container, Text, Title } from "@mantine/core"

import Layout from "../layouts/Layout"

function About() {
    return (
        <Layout>
            <Container size='xl'>
                <Title order={1} py='md'>About me</Title>
                <Text>
                    I'm a programmer / developer and tech enthusiast
                    with experience in web development, server-side programming, and Linux systems.
                    In my free time, I love watching movies and playing videogames including Microsoft Flight Simulator, Project Zomboid, RimWorld and Assetto Corsa.
                    I also enjoy playing around with shell scripts, Blender, game engines like Unity and Godot, and creating game addons.
                    I am always looking for new challenges and opportunities to learn and grow as a programmer and tech lover.
                </Text>
            </Container>
        </Layout>
    )
}

export default About