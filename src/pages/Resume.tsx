import { Link } from "react-router-dom";
import { ActionIcon, Container, Divider, Grid, Group, List, Table, Text, Title } from "@mantine/core";
import { FileDownload } from "tabler-icons-react";

import Layout from "../layouts/Layout";

function Resume() {
    return (
        <Layout>
            <Container size="xl">
                <Group py="md">
                    <Title order={1}>Jesse Vordzorgbe</Title>
                    <ActionIcon size={24} mt={10} component="a" href="./cv.pdf" download>
                        <FileDownload color="gray" />
                    </ActionIcon>
                </Group>
                <Table>
                    <tbody>
                        <tr>
                            <td>Date of Birth</td>
                            <td>09/01/2000</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>Antwerp, Belgium</td>
                        </tr>
                        <tr>
                            <td>Website</td>
                            <td>
                                <Link to="/">vordzorgbe.netlify.app</Link>
                            </td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>
                                <a href="tel:+32487982024">+32 487 98 20 24</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>
                                <a href="mailto:jesse.vordzorgbe@gmail.com">jesse.vordzorgbe&#64;gmail.com</a>
                            </td>
                        </tr>
                    </tbody>
                </Table>

                <Grid>
                    <Grid.Col lg={8}>
                        <div>
                            <Title order={2} pt="lg">
                                Education
                            </Title>
                            <Divider mt={5} />
                            <Title order={3} size="h4" pt="md">
                                Professional bachelor of Applied Computer Science
                            </Title>
                            <Text c="dimmed" fs="italic">
                                KdG University of Applied Sciences and Arts
                            </Text>
                            <Text c="dimmed" fs="italic">
                                2020 - 2024
                            </Text>

                            <Title order={3} size="h4" pt="md">
                                General Secondary Education
                            </Title>
                            <Text c="dimmed" fs="italic">
                                Stedelijk Lyceum Quellin
                            </Text>
                            <Text c="dimmed" fs="italic">
                                2012 - 2018
                            </Text>
                        </div>
                        <div>
                            <Title order={2} pt="lg">
                                Work Experience
                            </Title>
                            <Divider mt={5} />
                            <Title order={3} size="h4" pt="md">
                                Portfolio
                            </Title>
                            <Text c="dimmed" fs="italic">
                                Web development
                            </Text>
                            <Text c="dimmed" fs="italic">
                                12.2022 - Now
                            </Text>
                            <Text>
                                Implemented a CI/CD pipeline for a client side React application, which allowed for
                                automated building, and deployment of new features and updates. Currently working on
                                porting the application to Astro for SSR, SEO and content management.
                            </Text>

                            <Title order={3} size="h4" pt="md">
                                ASP.NET Web Application
                            </Title>
                            <Text c="dimmed" fs="italic">
                                Software development
                            </Text>
                            <Text c="dimmed" fs="italic">
                                11.2021
                            </Text>
                            <Text>
                                The development of an ASP.NET Core web application for the management of movies, actors
                                and roles with an SQLite backend.
                            </Text>
                        </div>
                    </Grid.Col>
                    <Grid.Col lg={4}>
                        <div>
                            {/* ADD DIVIDER */}
                            <Title order={2} pt="lg">
                                Technologies
                            </Title>
                            <Divider mt={5} />
                            <List listStyleType="square" pt="xs">
                                <List.Item>Typescript / Javascript</List.Item>
                                <List.Item>Node.js</List.Item>
                                <List.Item>React</List.Item>
                                <List.Item>Svelte</List.Item>
                                <Divider m={5} />
                                <List.Item>Java</List.Item>
                                <Divider m={5} />
                                <List.Item>.NET core</List.Item>
                                <Divider m={5} />
                                <List.Item>SQL (Oracle, SQLite, Postgres)</List.Item>
                                <Divider m={5} />
                                <List.Item>Bash</List.Item>
                            </List>
                        </div>
                        {/* <h2>Skills</h2> */}
                    </Grid.Col>
                    <Grid.Col></Grid.Col>
                </Grid>
            </Container>
        </Layout>
    );
}

export default Resume;
