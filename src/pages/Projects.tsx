import useSWR from 'swr'
import { Aside, Box, Container, Divider, Flex, MediaQuery, Title } from "@mantine/core"

import useFetch from "../hooks/useFetch"
import ProjectData from "../types/ProjectData"

import Layout from "../layouts/Layout"
import ProjectCard from "../components/ProjectCard"
import PersonalProgress from "../components/PersonalProgress"
import ProjectCardPlaceholder from "../components/ProjectCardPlaceholder"

function Projects() {
    const url = '/.netlify/functions/projects'
    const { data, isLoading, error } = useSWR(url, useFetch)
    return (
        <Layout>
            <Container size='xl'>
                <Title order={1} py='md'>Projects</Title>
                <MediaQuery largerThan='md' styles={{ display: 'none' }}>
                    <Box pb='md'>
                        <Divider mb='md' />
                        <PersonalProgress />
                        <Divider mt='md' />
                    </Box>
                </MediaQuery>
                {/* TODO: Extract into own component */}
                <Flex direction='column' gap='md'>
                    {isLoading ?
                        <>
                            <ProjectCardPlaceholder />
                            <ProjectCardPlaceholder />
                        </>
                        :
                        data.map((item: ProjectData, index: number) => (
                            <ProjectCard key={index} project={item} />
                        ))
                    }
                </Flex>
                <MediaQuery smallerThan='md' styles={{ display: 'none' }}>
                    <Aside width={{ md: 300 }} hiddenBreakpoint='md' p='xl'>
                        <PersonalProgress />
                    </Aside>
                </MediaQuery>
            </Container>
        </Layout >
    )
}

export default Projects