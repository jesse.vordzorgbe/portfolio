import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Button, Container, Flex, Grid, Group, Stack, Text, Textarea, TextInput, Title } from "@mantine/core"
import { ArrowRight } from "tabler-icons-react"

import Layout from "../layouts/Layout"

type submitOptions = 'beforeSubmit' | 'success' | 'error'

type errorProps = {
    setSubmitted: React.Dispatch<React.SetStateAction<submitOptions>>
}

function encode(data: any) {
    return Object.keys(data)
        .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
        .join('&')
}

function Contact() {
    const [submitted, setSubmitted] = useState<submitOptions>('beforeSubmit')
    const [formData, setFormData] = useState({
        firstName: '',
        lastName: '',
        email: '',
        message: ''
    })

    useEffect(() => {
        return () => {
            setSubmitted('beforeSubmit')
        }
    }, [])

    function handleSubmit(event: React.FormEvent) {
        event.preventDefault()
        fetch("/", {
            method: 'POST',
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
            body: encode({ "form-name": "contact", ...formData })
        })
            .then(() => {
                setSubmitted('success')
            })
            .catch((error) => {
                setSubmitted('error')
            })
    }

    if (submitted === 'success')
        return (
            <Layout>
                <Success />
            </Layout>
        )

    if (submitted === 'error') {
        return (
            <Layout>
                <Error setSubmitted={setSubmitted} />
            </Layout>
        )
    }

    return (
        <Layout>
            <Container size='xl'>
                <Title order={1} py='md'>Contact me</Title>
                <Text pb='xl'>
                    If you have any questions or would like to get in touch, don't hesitate to contact me using the form below.
                    Alternatively, you can contect me directly using any of the methods found on the left.
                </Text>
                <form name='contact' method='post' onSubmit={handleSubmit}>
                    <input type='hidden' name='form-name' value='contact' />
                    <Stack>
                        <Grid>
                            <Grid.Col lg={6}>
                                <TextInput label='First name' required value={formData.firstName} onChange={(e) => {
                                    setFormData(({ ...formData, firstName: e.currentTarget.value }))
                                    console.log(formData);
                                }} />
                            </Grid.Col>
                            <Grid.Col lg={6}>
                                <TextInput label='Last name' required value={formData.lastName} onChange={(e) => {
                                    setFormData(({ ...formData, lastName: e.currentTarget.value }))
                                }} />
                            </Grid.Col>
                        </Grid>
                        <TextInput label='Email' type='email' required value={formData.email} onChange={(e) => {
                            setFormData(({ ...formData, email: e.currentTarget.value }))
                        }} />
                        <Textarea label='Message' required autosize minRows={2} maxRows={4} value={formData.message} onChange={(e) => {
                            setFormData(({ ...formData, message: e.currentTarget.value }))
                        }} />
                        <Group py='lg'>
                            <Button type='submit' rightIcon={<ArrowRight size={18} />} w={100}>Send</Button>
                        </Group>
                    </Stack>
                </form>
            </Container>
        </Layout>
    )
}

const Success = () => {
    return (
        <Container size='xl' py='lg'>
            <Flex direction='column' justify='center' align='center'>
                <h1 style={{ marginBottom: 5 }}>Thank you!</h1>
                <Text c='dimmed' pb='md'>
                    Your message has been sent.
                </Text>
                <Button component={Link} to='/' variant='subtle'>Back to homescreen</Button>
            </Flex>
        </Container>
    )
}

const Error = ({ setSubmitted }: errorProps) => {
    return (
        <Container size='xl' py='lg'>
            <Flex direction='column' justify='center' align='center'>
                <h1 style={{ marginBottom: 5 }}>Looks like something went wrong</h1>
                <Text c='dimmed' pb='md'>
                    Your message was not sent.
                </Text>
                <Button onClick={() => setSubmitted('beforeSubmit')} variant='subtle'>Try again</Button>
            </Flex>
        </Container>
    )
}

export default Contact