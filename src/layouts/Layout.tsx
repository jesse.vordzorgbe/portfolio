import { useState } from "react"
import { Link } from "react-router-dom"
import { ActionIcon, AppShell, Burger, Button, Container, Divider, Flex, Group, Header, Image, MediaQuery, Navbar, Stack, Text, Title, useMantineColorScheme, useMantineTheme } from "@mantine/core"
import { Sun, Moon, Mail, BrandLinkedin, Phone, BrandGitlab } from "tabler-icons-react"

function Layout({ children }: any) {
    const { colorScheme, toggleColorScheme } = useMantineColorScheme()
    const theme = useMantineTheme()
    const [opened, setOpened] = useState(false)
    const navlinkColors = colorScheme === 'light' ? theme.colors.dark[4] : theme.colors.dark[0]


    const NavContent = () => {
        return (
            <Navbar hidden={!opened} hiddenBreakpoint='sm' width={{ sm: 300, lg: 400 }}>
                <Container p='xl'>
                    <Group noWrap>
                        <Image src='./jv.jpg' withPlaceholder width={75} height={75} radius='sm' />
                        <Title order={4}>Jesse Vordzorgbe</Title>
                    </Group>

                    <Text py='sm'>
                        I'm a motivated student looking to graduate with a bachelor in Applied Computer Science in the next year.
                        Passionate about movies and videogames.
                    </Text>

                    <Stack spacing='xs'>
                        <Divider />
                        <Text c={navlinkColors}
                            variant='link'
                            component={Link}
                            to='/'
                            fw='bold'>
                            Home
                        </Text>
                        <Divider />

                        <Text c={navlinkColors}
                            variant='link'
                            component={Link}
                            to='/resume'
                            fw='bold'>
                            Resume
                        </Text>
                        <Divider />

                        <Text c={navlinkColors}
                            variant='link'
                            component={Link}
                            to='/projects'
                            fw='bold'>
                            Projects
                        </Text>
                        <Divider />

                        <Text c={navlinkColors}
                            variant='link'
                            component={Link}
                            to='/interests'
                            fw='bold'>
                            Interests
                        </Text>
                        <Divider />

                        <Text c={navlinkColors}
                            variant='link'
                            component={Link}
                            to='/about'
                            fw='bold'>
                            About me
                        </Text>
                        <Divider />

                        <Text c={navlinkColors}
                            variant='link'
                            component={Link}
                            to='/contact'
                            fw='bold'>
                            Contact
                        </Text>
                        <Divider />
                    </Stack>

                    <Flex py='xs' align='center' justify='space-around' wrap='nowrap'>
                        <ActionIcon component='a' href='mailto:jesse.vordzorgbe@gmail.com'>
                            <Mail color='gray' />
                        </ActionIcon>
                        <ActionIcon component='a' href='tel:+32487982024'>
                            <Phone color='gray' />
                        </ActionIcon>
                        <ActionIcon component='a' href='https://www.linkedin.com/in/jesse-vordzorgbe-a52290257' target="_blank">
                            <BrandLinkedin color='gray' />
                        </ActionIcon>
                        <ActionIcon component='a' href='https://gitlab.com/jesse.vordzorgbe' target="_blank">
                            <BrandGitlab color='gray' />
                        </ActionIcon>
                    </Flex>
                </Container>
            </Navbar>
        )
    }

    const HeaderContent = () => {
        return (
            <Header height={50} p='md'>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', height: '100%' }}>
                    <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
                        <Burger
                            opened={opened}
                            onClick={() => setOpened((o) => !o)}
                            size="sm"
                        />
                    </MediaQuery>
                    <MediaQuery smallerThan='sm' styles={{ display: 'none' }}>
                        <Button compact component={Link} to='/'>Jesse Vordzorgbe</Button>
                    </MediaQuery>

                    <MediaQuery largerThan='sm' styles={{ display: 'none' }}>
                        <Image height={50} fit='contain' src='./nyan-cat.gif' />
                    </MediaQuery>

                    <ActionIcon variant='default' onClick={() => toggleColorScheme()} size={28}>
                        {colorScheme === 'light' ? <Sun size={18} /> : <Moon size={16} />}
                    </ActionIcon>
                </div>
            </Header>
        )
    }

    return (
        <AppShell navbarOffsetBreakpoint='sm'
            navbar={<NavContent />}
            header={<HeaderContent />}>
            {children}
        </AppShell>
    )
}

export default Layout