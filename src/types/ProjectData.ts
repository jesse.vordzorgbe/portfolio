type ProjectData = {
    title: string,
    url: string,
    description: string,
    image: string,
    technologies: string[]
}

export default ProjectData