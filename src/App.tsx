import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { useLocalStorage } from '@mantine/hooks'
import { ColorScheme, ColorSchemeProvider, MantineProvider } from '@mantine/core'

import Resume from './pages/Resume'
import NotFoundTitle from './pages/NotFoundTitle';
import Interests from './pages/Interests';
import Contact from './pages/Contact';
import Projects from './pages/Projects';
import About from './pages/About';
import Home from './pages/Home';

function App() {
  const [colorScheme, setColorScheme] = useLocalStorage<ColorScheme>({
    key: 'mantine-color-scheme',
    defaultValue: 'dark',
    getInitialValueInEffect: true,
  });

  function toggleColorScheme() {
    setColorScheme(colorScheme === 'dark' ? 'light' : 'dark')
  }

  return (
    <BrowserRouter>
      <ColorSchemeProvider colorScheme={colorScheme} toggleColorScheme={toggleColorScheme}>
        <MantineProvider withNormalizeCSS withGlobalStyles theme={{ colorScheme }} >
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/resume' element={<Resume />} />
            <Route path='/projects' element={<Projects />} />
            <Route path='/interests' element={<Interests />} />
            <Route path='/contact' element={<Contact />} />
            <Route path='/about' element={<About />} />
            <Route path='/*' element={<NotFoundTitle />} />
          </Routes>
        </MantineProvider>
      </ColorSchemeProvider>
    </BrowserRouter>
  )
}

export default App
