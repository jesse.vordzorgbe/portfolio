async function useFetch(url: string) {
    const res = await fetch(url)
    return await res.json()
}

export default useFetch